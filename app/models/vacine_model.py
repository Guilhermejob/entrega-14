from sqlalchemy.sql.sqltypes import Integer
from app.configs.database import db
from datetime import *

from sqlalchemy import Column, String, DateTime
from sqlalchemy.orm import validates
from dataclasses import dataclass


@dataclass
class VacineModel(db.Model):

    cpf: int
    name: str
    first_shot_date: str
    second_shot_date: str
    vaccine_name: str
    health_unit_name: str

    @validates('cpf', 'name', 'first_shot_date', 'second_shot_date', 'vaccine_name', 'health_unit_name')
    def convert_upper(self, key, value):
        return value.upper()

    __tablename__ = 'vaccine_cards'

    cpf = Column(String, primary_key=True)
    name = Column(String, nullable=False)
    first_shot_date = Column(DateTime, default=datetime.now())
    second_shot_date = Column(
        DateTime, default=datetime.now() + timedelta(hours=1))
    vaccine_name = Column(String, nullable=False)
    health_unit_name = Column(String)
    teste = Column(Integer, default=(1))
