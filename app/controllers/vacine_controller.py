from flask import request, current_app, jsonify
from app.models.vacine_model import VacineModel
from app.exec.exec import NotNumerical, NotElevenDigits, TypeNotAllowedError, MissingField
from sqlalchemy.exc import IntegrityError

from app.routes import vacine_blueprint


def create_vacine_card():

    required_keys = ['cpf', 'name', 'vaccine_name', 'health_unit_name']

    data_to_be_verified = request.get_json()

    data = {}

    for key in data_to_be_verified.keys():
        if key not in required_keys:
            pass
        else:
            data[key] = data_to_be_verified[key]

    try:
        if len(data.keys()) < len(required_keys):
            raise MissingField

        for value in data.values():
            if type(value) != str:
                raise TypeNotAllowedError(**data)

        if not data['cpf'].isdecimal():
            raise NotNumerical

        if len(data['cpf']) < 11 or len(data['cpf']) > 11:
            raise NotElevenDigits

        try:
            new_vacine_card = VacineModel(**data)

            current_app.db.session.add(new_vacine_card)
            current_app.db.session.commit()

            return jsonify(new_vacine_card), 201
        except IntegrityError as err:
            return jsonify({'error': 'cpf already exists'}), 409
    except (TypeNotAllowedError, NotElevenDigits, NotNumerical, MissingField) as err:
        return jsonify(err.message), 400


def get_all():
    vacines = (
        VacineModel
        .query
        .all()
    )

    serializer = [
        {
            "cpf": vacine.cpf,
            "name": vacine.name,
            "first_shot_date": vacine.first_shot_date,
            "second_shot_date": vacine.second_shot_date,
            "vaccine_name": vacine.vaccine_name,
            "health_unit_name": vacine.health_unit_name,

        } for vacine in vacines
    ]

    return jsonify([serializer]), 200
