class MissingField(Exception):
    def __init__(self):
        self.message = {
            'err': 'some field is missing',
            'required_fields': 'cpf, name, vaccine_name, health_unit_name'
        }
        super().__init__(self.message)


class NotNumerical(Exception):

    def __init__(self):
        self.message = {
            'err': 'The cpf field only accepts numeric characters'
        }
        super().__init__(self.message)


class NotElevenDigits(Exception):
    def __init__(self):
        self.message = {
            'err': 'The CPF field must be eleven digits'
        }
        super().__init__(self.message)


class TypeNotAllowedError(Exception):
    types = {
        str: "string",
        int: "integer",
        float: "float",
        list: "list",
        dict: "dictionary",
        bool: "boolean",
    }

    def __init__(self, cpf, name, vaccine_name, health_unit_name):

        self.message = {
            'wrong fields': [
                {
                    "cpf": f'{self.types[type(cpf)]}'
                },
                {
                    "name": f'{self.types[type(name)]}'
                },
                {
                    "vaccine_name": f'{self.types[type(vaccine_name)]}'
                },
                {
                    "name": f'{self.types[type(health_unit_name)]}'
                },
                {
                    "wrong": 'Some field is not a string'
                }
            ]
        }
