from flask import Blueprint
from sqlalchemy.sql.ddl import CreateColumn

from app.controllers.vacine_controller import create_vacine_card, get_all

bp_vacines = Blueprint("bp_vacines", __name__, url_prefix='/vaccinations')

bp_vacines.post('')(create_vacine_card)
bp_vacines.get('')(get_all)
