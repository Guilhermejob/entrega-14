from flask import Blueprint
from flask.app import Flask
from app.routes.vacine_blueprint import bp_vacines


def init_app(app: Flask):

    app.register_blueprint(bp_vacines)
